import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { A } from '@ember/array';
import NewItem from '../utils/new-item';
import faker from 'faker';
import EmberObject from '@ember/object';

export default Route.extend({

  model(){

    var itemsArray = A ([]);

    for(let i=0; i< 100; i++){
      let dates = A([]);

      for(let i = 0; i < 3; i++){
        var dateObject = new EmberObject();
        let randomDate = new Date(faker.date.between('2019-01-01', '2019-12-31'))+""; 
        dateObject.set("date",randomDate);
        dates.push(dateObject);
      }

      let item = NewItem.create();
      item.set("firstName",faker.name.firstName());
      item.set("lastName",faker.name.lastName());
      item.set("dates",dates);

      itemsArray.push(item);
      if(i % 4 === 0){
        item.set("disabled",true);

      }
    }

    return RSVP.hash({
      items:
        itemsArray

    });
    }
});
