import Controller from '@ember/controller';
import MultisearchFilterMixin from 'ember-multisearch-sortable-tables/mixins/multisearch-filter-mixin';
import SortableTableColumnMixin from 'ember-multisearch-sortable-tables/mixins/sortable-table-column-mixin';
import { A } from '@ember/array';

export default Controller.extend(MultisearchFilterMixin,SortableTableColumnMixin,{
    filterObjectsTarget: 'application',
    searchFilters: null,
    sortBy: null,
    queryParams: ['date'],

  init() {
    this._super(...arguments);

    var filters = [
      {
        searchTarget: 'firstName',
        searchPlaceHolder: 'First Name',
        checked: false
      },
      ,
      {
        searchTarget: 'disabled',
        searchPlaceHolder: 'Disabled',
        type: 'checkbox',
        columnSize: 3,
        checkboxValue: true,
        checked: false
      },

      {
        queryParam: 'date',
        searchPlaceHolder: 'Submitted (After)',
        searchTarget: 'dates',
        searchPlaceHolder: 'Disabled',
        operation: '<',
        type: 'date',
        checked: true,
        rawValue: false,
        dateValue: null,
        isHasMany: true,
        optionValuePath: "date"
      }
    ];

    this.setSearchFilters(filters);
    this.set('sortBy', A(['name']));

  }

});
